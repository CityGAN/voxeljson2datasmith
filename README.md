"""
Transform Voxelgraph output to datasmith via obj and Blender in commandline to get the Voxelgraph output into Unreal Engine
This package uses the Blender Datasmith plugin. 
It works. I am not a programmer, so forgive me for the missing elegance. 
"""

Installations and Prerequisites:

1 Windows 10

2 Python 3
    https://www.python.org/downloads/

3 blender 2.92 or above
    https://www.blender.org/download/
    Add Blender to PATH in Win10

4 datasmith plugin 
    https://github.com/0xafbf/blender-datasmith-export
	installation:
	Blender > Edit > Preferences > Add-ons > Install > click the plugin.zip > make sure the tickbox of the plugin is set to active	

5 define input file and output path in args.json
    replace all "\" in the path names with "\\" for things to work

6 The following files need to be in the same folder:
    Blender_VG_to_DS.py 
    Empty_for_VG_import.blend
    args.json


This is the cmd.exe syntax to run Blender headlessly and fire a python script from within:
    
    blender scene.blend -b -P script.py


After running the command you should find four things in the output folder (defined in args.json):
inputfilename.obj
inputfilename.mtl
inputfilename.udatasmith
Folder called inputfilename_Assets

In Unreal you need to select the .udatasmithfile via the datasmith import workflow

