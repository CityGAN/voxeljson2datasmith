# Python
""" 
This converts voxel-graph data as described in 
https://openaccess.thecvf.com/content/ICCV2021/papers/Chang_Building-GAN_Graph-Conditioned_Architectural_Volumetric_Design_Generation_ICCV_2021_paper.pdf
to obj files for downstream use. The second half of the script uses Blender with datagraph export plugin to create .fbx and datasmith formatted files from the objs.

"""
# Date:     2022 05 25

# import bpy will onyl work when script is started from within BLENDER.
import bpy  

import os
import json
import sys
from datetime import datetime


global_counter = 0
script_version = "1.0"
now = datetime.now()

script_directory = os.path.dirname(__file__)

print("__________START DOING STUFF!___________")
print("Script Direcory is: " + str(script_directory))


# Function to convert a single voxel-graph item into obj-like cube
def voxel_to_obj():
    global global_counter

    type_map = {'0': 'LobbyCorridor', '1': 'Restroom', '2': 'Stairs', '3': 'Elevator', '4': 'Office', '5': 'Mechanical room', '-1': 'Void', '-2': 'NotAllowed'}
    room_type = str(one_box_content["type"])
    room_name = type_map[room_type]
    
    if room_name == type_map["-1"] or room_name == type_map["-2"]:
        pass
    elif int(room_type) in range(6):

        xMin = str(float(one_box_content["coordinate"][2]))
        xMax = str(float(one_box_content["coordinate"][2]) + float(one_box_content["dimension"][2]))
        yMin = str(float(one_box_content["coordinate"][1]) + float(one_box_content["dimension"][1]))
        yMax = str(float(one_box_content["coordinate"][1]))
        zMin = str(float(one_box_content["coordinate"][0]) + float(one_box_content["dimension"][0]))
        zMax = str(float(one_box_content["coordinate"][0]))
        

        #box_name = "Box:" + str(one_box_content_"locationX"_"locationY"_"locationZ")

        box_location = str(one_box_content["location"])  
        box_location_clean = box_location.replace("[", "")
        box_location_clean = box_location_clean.replace("]", "")
        box_location_clean = box_location_clean.replace(",", "_")
        box_name = "Box_" + box_location_clean
        v1 = str("v " + xMin + " " + yMin + " " + zMin)
        v2 = str("v " + xMax + " " + yMin + " " + zMin)
        v3 = str("v " + xMax + " " + yMax + " " + zMin)
        v4 = str("v " + xMin + " " + yMax + " " + zMin)
        v5 = str("v " + xMin + " " + yMin + " " + zMax)
        v6 = str("v " + xMin + " " + yMax + " " + zMax)
        v7 = str("v " + xMax + " " + yMax + " " + zMax)
        v8 = str("v " + xMax + " " + yMin + " " + zMax)

        vertices = str(v1) +"\n" + str(v2) +"\n"+ str(v3) +"\n"+ str(v4) +"\n"+ str(v5) +"\n"+ str(v6) +"\n"+ str(v7) +"\n"+ str(v8) +"\n"

        corner_1 = str( 1 + ( 8 * global_counter))
        corner_2 = str( 2 + ( 8 * global_counter))
        corner_3 = str( 3 + ( 8 * global_counter))
        corner_4 = str( 4 + ( 8 * global_counter))
        corner_5 = str( 5 + ( 8 * global_counter))
        corner_6 = str( 6 + ( 8 * global_counter))
        corner_7 = str( 7 + ( 8 * global_counter))
        corner_8 = str( 8 + ( 8 * global_counter))

        faces = str(
            "f " + corner_1 + " "  + corner_4 + " "  + corner_3 + " "  + corner_2 +"\nf "
            + corner_8 + " " + corner_7 + " " + corner_6 + " " + corner_5 + "\nf "
            + corner_2 + " " + corner_3 + " " + corner_7 + " " + corner_8 + "\nf " 
            + corner_3 + " " + corner_4 + " " + corner_6 + " " + corner_7 + "\nf " 
            + corner_4 + " " + corner_1 + " " + corner_5 + " " + corner_6 + "\nf " 
            + corner_1 + " " + corner_2 + " " + corner_8 + " " + corner_5
            )
                    
        obj_box = str(vertices + faces)

        obj_file = open(obj_path, 'a')
        obj_file.write("\n" "usemtl " + room_name + "\ns off\n" + "o " + box_name + "\n" + obj_box)
        obj_file.close()
        
        global_counter += 1
    else:
        print("This data contains unmapped room types.")



# Get User specified Voxelgraph Input file and desired output folder path from args.json

args_json_full_path = str(os.path.dirname(__file__)) + "\\args.json"

with open(args_json_full_path, "r") as args_file:
    user_args = json.load(args_file)

    input_file = user_args["input_file"]
    out_folder = user_args["out_folder"]

    print("THE INPUT FILE IS: " + str(input_file) + "\n \nTHE OUTPUT FOLDER IS: " + str(out_folder))

args_file.close()

vg_name = os.path.basename(input_file)
print("vg_name: " + str(vg_name))

obj_name = os.path.splitext(vg_name)[0]
print("obj_name: " + str(obj_name))
 

# Open the Voxel Graph file and write the obj into the defined output folder

with open(input_file) as infile:
    input_file_content = json.load(infile)
voxel_graph_list = input_file_content["voxel_node"]


obj_path = out_folder + "\\" + str(obj_name) + ".obj"
print("obj path: " + str(obj_path))

obj_file = open(obj_path, 'w')
#print("obj file: " + str(obj_file))

obj_file.write("""# VoxelGraphToOBJ output
# Date:         """ + str(now) +
"\n" + "# Version:      " + str(script_version) + "\n" + "mtllib " + str(obj_name) + ".mtl")
obj_file.close()

# iterate conversion for each box
for one_box_content in voxel_graph_list:
    voxel_to_obj()

# write mtl file
mtl_path = out_folder + "\\" + str(obj_name) +  ".mtl"
mtl_file = open(mtl_path, 'w')
mtl_file.write("""# VoxelGraphToOBJ script output
# Date:             """ + str(now) + """
# Version:          """ + str(script_version) + """
# Material Count:   8\n""")
mtl_file.write("""newmtl LobbyCorridor
Ns 323.999994
Kd 0.772549 0.619608 0.46666
Ka 0.800000 0.800000 0.800000
Ks 0.500000 0.500000 0.500000
Ke 0.000000 0.000000 0.000000
Ni 1.450000
d 1.000000
illum 2
newmtl Restroom
Ns 323.999994
Kd 0.894118 0.231373 0.501961
Ka 0.800000 0.800000 0.800000
Ks 0.500000 0.500000 0.500000
Ke 0.000000 0.000000 0.000000
Ni 1.450000
d 1.000000
illum 2
newmtl Stairs
Ns 323.999994
Kd  0.941176 0.898039 0.152941
Ka 0.800000 0.800000 0.800000
Ks 0.500000 0.500000 0.500000
Ke 0.000000 0.000000 0.000000
Ni 1.450000
d 1.000000
illum 2
newmtl Elevator
Ns 323.999994
Kd 0.235294 0.713725 0.380392
Ka 0.800000 0.800000 0.800000
Ks 0.500000 0.500000 0.500000
Ke 0.000000 0.000000 0.000000
Ni 1.450000
d 1.000000
illum 2
newmtl Office
Ns 323.999994
Kd 0.109804 0.498039 0.764706
Ka 0.800000 0.800000 0.800000
Ks 0.500000 0.500000 0.500000
Ke 0.000000 0.000000 0.000000
Ni 1.450000
d 1.000000
illum 2
newmtl Mechanical
Ns 323.999994
Kd 0.988235 0.568627 0.152941
Ka 0.800000 0.800000 0.800000
Ks 0.500000 0.500000 0.500000
Ke 0.000000 0.000000 0.000000
Ni 1.450000
d 1.000000
illum 2")""")
mtl_file.close()

print("Obj Done")

# The following stuff requires Blender and its datamsith exporter plugin to work. 

""" Open Empty Scene """
# filepath should point to the "Empty_for_VG_import.blend"-Scene, that is provided with the package
print("__________START OPEN EMPTY SCENE!___________")
empty_scene = script_directory + str("\\Empty_for_VG_import_220406.blend")
print("Empty Scene is: " + str(empty_scene))
bpy.ops.wm.open_mainfile(filepath = empty_scene)
#bpy.ops.wm.open_mainfile(filepath="C:\\Users\\Alex.MININT-59EP0QD\\Desktop\\VoxelGraph_220214\\BlenderEmptyScene\\Empty_for_VG_import_220406.blend")
print("__________OPEN EMPTY SCENE DONE!___________")


""" Import OBJ to empty Scene """
# import the obj just created into the empty blender scene
print("__________START IMPORT OBJ!___________")
bpy.ops.import_scene.obj(filepath=obj_path,filter_glob='*.obj;*.mtl', use_edges=True, use_smooth_groups=True, use_split_objects=True, use_split_groups=False, use_groups_as_vgroups=False, use_image_search=True, split_mode='ON', global_clamp_size=0.0, axis_forward='-Y', axis_up='Z')
print("__________IMPORT OBJ DONE!___________")


""" Export FBX """
#print("__________START EXPORT FBX!___________")
#bpy.ops.export_scene.fbx(filepath = str(script_directory) + "Script_Export.fbx", check_existing = True, filter_glob = '*.fbx', use_selection = False, use_active_collection = False, global_scale = 1.0, apply_unit_scale = True, apply_scale_options = 'FBX_SCALE_NONE', use_space_transform = True, bake_space_transform = False, object_types = {'ARMATURE', 'CAMERA', 'EMPTY', 'LIGHT', 'MESH', 'OTHER'}, use_mesh_modifiers = True, use_mesh_modifiers_render = True, mesh_smooth_type = 'OFF', use_subsurf = False, use_mesh_edges = False,use_tspace = False, use_custom_props = False, add_leaf_bones = True, primary_bone_axis = 'Y', secondary_bone_axis = 'X', use_armature_deform_only = False, armature_nodetype = 'NULL', bake_anim = True, bake_anim_use_all_bones = True, bake_anim_use_nla_strips = True, bake_anim_use_all_actions = True, bake_anim_force_startend_keying = True, bake_anim_step = 1.0, bake_anim_simplify_factor = 1.0, path_mode = 'AUTO', embed_textures = False, batch_mode = 'OFF', use_batch_own_dir = True, use_metadata = True, axis_forward = '-Y', axis_up = 'Z')
#print("__________EXPORT FBX DONE!___________")

""" Export DATASMITH """
print("__________START EXPORT DATASMITH!___________")
bpy.ops.export_scene.datasmith(filepath = str(out_folder) + "\\" + str(obj_name) + ".fbx")
#out_folder + "\\" + str(obj_name) + ".fbx"
print("__________EXPORT DATASMITH DONE!___________")

print("Everything DONE!")